'use strict';

const idsToZero = [202, 203, 208, 209, 210, 214]

const idsPrevValues = {}

/**
 * Store currently set values for later
 * @param {int} unitCode the ships unit code e.g. 202 for light cargo
 * @param {int} [column] the attacker (0) or defender (1) column of the simulation, default to 0
 */
function saveCurrentValues(unitCode, column = 0) {
  const elms = document.getElementsByName(`battleinput[0][${column}][${unitCode}]`)
  if (elms.length === 1) {
    const e = elms[0]
    idsPrevValues[unitCode] = e.value
  }
}

/**
 * Set value of a given ship type to 0
 * @param {int} unitCode the ships unit code e.g. 202 for light cargo
 * @param {int} [column] the attacker (0) or defender (1) column of the simulation, default to 0
 */
function setFieldToZero(unitCode, column = 0) {
  const elms = document.getElementsByName(`battleinput[0][${column}][${unitCode}]`)
  if (elms.length === 1) {
    const e = elms[0]
    e.value = 0
  }
}

/**
 * Set attackers ship type count to maximum number of available units
 * @param {int} unitCode the ships unit code e.g. 202 for light cargo
 * @param {bool} [fakeAsButton] add alt text and turn cursor into pointer for the label
 */
function maxValueOnLabelClick(unitCode, fakeAsButton = true) {
  const elms = document.getElementsByName(`battleinput[0][0][${unitCode}]`)
  if (elms.length === 1) {
    const e = elms[0]
    const maxValue = idsPrevValues[unitCode]
    const labelElm = document.evaluate('../../td', e, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue
    labelElm.addEventListener('click', () => {
      e.value = maxValue
    })
    if (fakeAsButton) {
      labelElm.style.cursor = 'pointer'
      labelElm.title = 'Click to reset to MAX'
    }
  }
}

function main() {
  for (let id = 202; id <= 215; id++) {
    saveCurrentValues(id)
    maxValueOnLabelClick(id)
  }
  for (const id of idsToZero) {
    setFieldToZero(id)
  }
}

main()