'use strict';

const {
  ExpoParser,
  languageMap
} = require('./expoParser')
const Chart = window['Chart']
const palette = require('google-palette')
let chartObj
const ep = new ExpoParser()

function updateExpoDB() {
  const expos = ep.getMessages().map(m => ep.parse_text(m))
  const expoDB = GM_getValue("expos", [])
  const existingExposIds = expoDB.map(e => e.id + e.date.split("T")[0]) // match by id AND date in case game gets reset and ids are starting from 0 again
  let added = 0
  for (const expo of expos) {
    if (existingExposIds.indexOf(expo.id + expo.date.split("T")[0]) < 0) {
      expoDB.push(expo)
      added += 1
    }
  }
  GM_setValue("expos", expoDB)
  console.log(`Added ${added} new expos to database, now contains ${expoDB.length} entries`)
}

function createChart(type = "bar") {
  const backgroundPlugin = {
    id: 'custom_canvas_background_color',
    beforeDraw: (chart) => {
      const ctx = chart.canvas.getContext('2d')
      ctx.save()
      ctx.globalCompositeOperation = 'destination-over'
      ctx.fillStyle = "rgba(250, 250, 250, 0.9)"
      ctx.fillRect(0, 0, chart.width, chart.height)
      ctx.restore()
    }
  }
  const ctx = document.getElementById('charts').getContext('2d')
  chartObj = new Chart(ctx, {
    type,
    data: {},
    options: {
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true,
          beginAtZero: true
        }
      },
      responsive: true,
      maintainAspectRatio: false
    },
    plugins: [
      backgroundPlugin
    ]
  })
  return chartObj
}

function GM_addStyle(css) {
  const style = document.getElementById("GM_addStyleBy8626") || (function () {
    const style = document.createElement('style');
    style.type = 'text/css';
    style.id = "GM_addStyleBy8626";
    document.head.appendChild(style);
    return style;
  })();
  const sheet = style.sheet;
  sheet.insertRule(css, (sheet.rules || sheet.cssRules || []).length);
}

function addChart() {
  GM_addStyle(".gimmecharts-canvas { width: 100%; height: 400px; margin: 3px; }");
  const d = document.createElement("div")
  const c = document.createElement("canvas")
  d.appendChild(c)
  d.classList = "gimmecharts-canvas"
  c.id = "charts"
  const content = document.getElementsByTagName("content")[0]
  content.insertAdjacentElement("beforeend", d)

  function createOption(value, text) {
    const e = document.createElement("option")
    e.value = value
    e.text = text
    return e
  }
  const s = document.createElement("select")
  s.id = "chartSelect"
  s.addEventListener("change", (event) => draw(event.target.value))
  s.appendChild(createOption("dailyRes", "Daily Resources"))
  s.appendChild(createOption("expoType", "Expo Events"))
  s.appendChild(createOption("dailyShipsAcc", "Daily Ships (accumulated)"))
  s.appendChild(createOption("exposPerDay", "Expos Count per Day (stacked)"))

  d.insertAdjacentElement("beforebegin", s)

  // create chart object
  chartObj = createChart("bar")

}

function getChartDataEventType() {
  const expoDB = GM_getValue("expos")
  const ep = new ExpoParser()
  const expoTypes = Object.keys(ep.expoTypeMessages)
  const datasets = []
  const uniquePlanets = [...new Set(expoDB.map(e => e.planet))]
  const colors = palette('mpn65', uniquePlanets.length).map(hex => `#${hex}`)
  let i = 0
  for (const planet of uniquePlanets) {
    const data = []
    const expos = expoDB.filter(e => e.planet == planet)
    for (const event of expoTypes) {
      const count = expos.filter(e => e.expoType == event).length
      data.push(count)
    }
    datasets.push({
      label: planet,
      data,
      backgroundColor: colors[i]
    })
    i += 1
  }
  let labels = expoTypes.map(t => t.replace("_", " "))
  labels = labels.map(l => l.split(" ").map(w => w.charAt(0).toUpperCase() + w.slice(1)).join(" "))
  const data = {
    labels,
    datasets
  }
  return data
}

function getChartDataDailyRes() {
  const expoDB = GM_getValue("expos")
  const dates = expoDB.map(e => e.date.split("T")[0])

  const uniqueDates = [...new Set(dates)].sort()
  const resTypes = ["Metal", "Crystal", "Deuterium"]
  const datasets = []
  const expos = expoDB.filter(e => e.expoType.includes("res_"))
  for (const resType of resTypes) {
    const data = []
    for (const date of uniqueDates) {
      const sum = expos
        .filter(e => e.date.split("T")[0] == date)
        .filter(e => e.details.type == resType)
        .map(e => e.details.count)
        .reduce((p, c) => p + c, 0)
      data.push(sum)
    }
    let color = "#000"
    switch (resType) {
      case "Metal":
        color = "#666"
        break
      case "Crystal":
        color = "#d3d"
        break
      case "Deuterium":
        color = "#0d7"
    }
    datasets.push({
      label: resType,
      data,
      backgroundColor: [color]
    })
  }

  let labels = uniqueDates
  const data = {
    labels,
    datasets
  }
  return data
}

function getChartDataDailyShipsAcc() {
  const expoDB = GM_getValue("expos")
  const dates = expoDB.map(e => e.date.split("T")[0])

  const uniqueDates = [...new Set(dates)].sort()
  const shipTypes = Object.values(languageMap.ships_en)
  shipTypes.push("Battle cr") // HOTFIX: Remove later - bug that caused wrong battle cruiser detection, fixed on 2022-05-11
  const datasets = []
  const expos = expoDB.filter(e => e.expoType.includes("ship_"))
  const colors = palette('mpn65', shipTypes.length).map(hex => `#${hex}`)
  let i = 0
  for (const shipType of shipTypes) {
    const data = []
    let cumSum = 0
    for (const date of uniqueDates) {
      const sum = expos
        .filter(e => e.date.split("T")[0] == date)
        .filter(e => Object.keys(e.details).includes(shipType))
        .map(e => e.details[shipType])
        .reduce((p, c) => p + c, cumSum)
      data.push(sum)
      cumSum = sum
    }
    if (cumSum > 0) {
      let label = 'Unknown'
      const ix = Object.values(languageMap.ships_en).indexOf(shipType)
      if (ix < 0) {
        if (shipType === 'Battle cr') { // HOTFIX: Remove later - bug that caused wrong battle cruiser detection, fixed on 2022-05-11
          label = 'Battle Cruiser'
        }
      } else {
        label = Object.keys(languageMap.ships_en)[ix]
      }
      datasets.push({
        label,
        data,
        borderColor: colors[i],
        backgroundColor: colors[i],
      })
      i += 1
    }
  }

  let labels = uniqueDates
  const data = {
    labels,
    datasets
  }
  return data
}

function getChartDataExposPerDay() {
  const expoDB = GM_getValue("expos")
  const dates = expoDB.map(e => e.date.split("T")[0])
  const uniqueDates = [...new Set(dates)].sort()
  const datasets = []

  const uniquePlanets = [...new Set(expoDB.map(e => e.planet))]
  const colors = palette('mpn65', uniquePlanets.length).map(hex => `#${hex}`)
  let i = 0
  for (const planet of uniquePlanets) {
    const data = []
    const expos = expoDB.filter(e => e.planet == planet)
    for (const date of uniqueDates) {
      const exposPday = expos
        .filter(e => e.date.split("T")[0] == date)
      data.push(exposPday.length)
    }
    datasets.push({
      label: planet,
      data,
      borderColor: colors[i],
      backgroundColor: colors[i],
    })
    i += 1
  }
  let labels = uniqueDates
  const data = {
    labels,
    datasets
  }
  return data
}

function draw(usecase) {
  // console.log("Drawing usecase: ", usecase)

  let data, chartType, isStacked = true
  switch (usecase) {
    case "expoType":
      data = getChartDataEventType()
      chartType = "bar"
      break
    case "dailyRes":
      data = getChartDataDailyRes()
      chartType = "bar"
      isStacked = false
      break
    case "dailyShipsAcc":
      data = getChartDataDailyShipsAcc()
      chartType = "line"
      isStacked = false
      break
    case "exposPerDay":
      data = getChartDataExposPerDay()
      chartType = "line"
      isStacked = true
  }

  if (chartType != chartObj.type) {
    chartObj.destroy()
    chartObj = createChart(chartType)
  }
  chartObj.data = data
  chartObj.options.scales.x.stacked = isStacked
  chartObj.options.scales.y.stacked = isStacked
  chartObj.update()
}

if (ep.isExpoPage()) {
  updateExpoDB()
  addChart()
  draw(document.getElementById("chartSelect").value)
}